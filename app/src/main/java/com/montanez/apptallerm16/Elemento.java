package com.montanez.apptallerm16;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class Elemento extends RecyclerView.ViewHolder {

        TextView tvElemento;

// Constructor

        public Elemento(@NonNull View itemV){
            super(itemV);
            tvElemento = itemV.findViewById(R.id.tvElemento);

        }
    }