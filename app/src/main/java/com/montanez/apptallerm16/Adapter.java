package com.montanez.apptallerm16;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Adapter extends RecyclerView.Adapter <Elemento> {

    //de donde obtiene los datos
    List<String> elementos;

    //construct

    public Adapter(List<String> datos){ elementos = datos;    }


    @NonNull
    @Override
    public Elemento onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //LayoutInflater mycreateBox;
        View myBox;
        //mycreateBox =
        myBox = LayoutInflater.from(parent.getContext()).inflate(R.layout.elemento, parent, false);
        return  new Elemento(myBox);
    }

    @Override
    public void onBindViewHolder(@NonNull Elemento holder, int position) {
        String myData = elementos.get(position);
        holder.tvElemento.setText(myData);


    }

    @Override
    public int getItemCount() {
        if(elementos!= null){
            return elementos.size();
        }else {
            return 0;
        }
    }
}