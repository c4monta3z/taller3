package com.montanez.apptallerm16;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity  {

    Button btnIngresar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnIngresar = findViewById(R.id.btnIngresar);

    btnIngresar.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View view)
        {
            // Acción a realizar cuando se presione el botón
            Intent  i = new Intent(MainActivity.this, secondPageRv.class) ;
            startActivity(i);
        }
    });



    }



}
