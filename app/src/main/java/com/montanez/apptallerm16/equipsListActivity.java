package com.montanez.apptallerm16;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AppCompatActivity;

public class equipsListActivity  extends AppCompatActivity {

    WebView equipWebPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.equips_list);
        equipWebPage = findViewById(R.id.equipWebPage);
        Intent myIntent = getIntent();
        String data = myIntent.getStringExtra("equipName");

        data.replaceAll(" ", "_");
        String url="https://es.wikipedia.org/wiki/"+data;
       equipWebPage.setWebViewClient(new WebViewClient());
        equipWebPage.loadUrl(url);





    }

}
