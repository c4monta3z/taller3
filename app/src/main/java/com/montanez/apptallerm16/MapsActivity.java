package com.montanez.apptallerm16;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import static android.content.ContentValues.TAG;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

public class MapsActivity extends AppCompatActivity {

    MapView mapa;
    IMapController mapaControlador;;
    MyLocationNewOverlay miUbicacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        mapa = (MapView) findViewById(R.id.mapa);

        //Obtener el contexto
        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        //Tile: Teselas, baldosas,
        mapa.setTileSource(TileSourceFactory.MAPNIK);

        //Permite controlar zoom con dedos
        mapa.setMultiTouchControls(true);

        //Explícitamente pedir los permisos
        //Arreglos de cadadenos de texto en donde guardo los permisos

        String[] permisos= new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permisos,1);
        }

        //Colocar un marcador en el sitio que me marque el "GPS"
        //Controlar el mapa
        mapaControlador=mapa.getController();

        miUbicacion= new MyLocationNewOverlay(new GpsMyLocationProvider(this), mapa);
        //Habilitar
        miUbicacion.enableMyLocation();
        mapa.getOverlays().add(miUbicacion);

        miUbicacion.runOnFirstFix(
                () -> {
                    runOnUiThread(
                            ()->{
                                mapaControlador.setZoom((double) 10);
                                mapaControlador.setCenter(miUbicacion.getMyLocation());
                                Log.d(TAG, "Ubicado!!!" + miUbicacion.getMyLocation().getLatitude() +
                                        miUbicacion.getMyLocation().getLongitude());
                            });
                }
        );
    }



        /*

        //Colocar un marcador en el mapa
        GeoPoint miPunto= new GeoPoint(4.62, -74.18);
        Marker miMarcador=new Marker(mapa);
        miMarcador.setPosition(miPunto);
        mapa.getOverlays().add(miMarcador);


        GeoPoint miPunto2= new GeoPoint(4.62, -74.20);
        Marker miMarcador2=new Marker(mapa);
        miMarcador2.setPosition(miPunto2);
        mapa.getOverlays().add(miMarcador2);
        mapaControlador.setZoom((double) 10);
        mapaControlador.animateTo(miPunto);


        */


}