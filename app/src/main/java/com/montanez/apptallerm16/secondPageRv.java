package com.montanez.apptallerm16;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.Arrays;
import java.util.List;

public class secondPageRv extends AppCompatActivity {

            RecyclerView rvElements;
            Adapter  myAdapter;
            Button btnMapa, btnCalc, btnEquipos;

            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_second_page_rv);
                btnMapa = findViewById(R.id.btnMapa);
                btnCalc = findViewById(R.id.btnCalc);
                btnEquipos = findViewById(R.id.btnEquipos);

                //obten the RV
                rvElements = findViewById(R.id.rvElementos);
                rvElements.setLayoutManager(new LinearLayoutManager(this));

                //definitions data and obtein data

                // work definitions data String Arrays
                List<String> myLocalData = Arrays.asList(getResources().getStringArray(R.array.nombres));

                //Create class object
                myAdapter = new Adapter(myLocalData);
                rvElements.setAdapter(myAdapter);


                btnMapa.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view)
                    {  Intent i = new Intent(secondPageRv.this, MapsActivity.class) ;
                        startActivity(i);
                    }
                });


                btnEquipos.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    { Intent i = new Intent(secondPageRv.this, EquiposFutActivity.class) ;
                        startActivity(i);
                    }
                });

                btnEquipos.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    { Intent i = new Intent(secondPageRv.this, EquiposFutActivity.class) ;
                        startActivity(i);
                    }
                });


                btnCalc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    { Intent i = new Intent(secondPageRv.this, CalculatorActivity.class) ;
                        startActivity(i);
                    }
                });


            }




        }