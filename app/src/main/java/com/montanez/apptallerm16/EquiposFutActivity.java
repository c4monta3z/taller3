package com.montanez.apptallerm16;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class EquiposFutActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    ListView lstVEquipos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipos_fut);
        lstVEquipos = findViewById(R.id.all_equips);

        String[] myEquips = getResources().getStringArray(R.array.equips);
        ArrayAdapter charguerEquips = new ArrayAdapter(this, R.layout.equip, myEquips);
        lstVEquipos.setAdapter(charguerEquips);
        lstVEquipos.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View v, int i, long l) {
        String nameEquip = ((TextView) v).getText().toString();
        Toast.makeText(getBaseContext(),nameEquip, Toast.LENGTH_LONG).show();
       Snackbar.make(findViewById(R.id.all_equips), nameEquip, Snackbar.LENGTH_LONG).show();
        Intent objective = new Intent(getBaseContext(), equipsListActivity.class);
        //Set data other Activity START
        //objective.putExtra("equipName",nameEquip);
        startActivity(objective);
    }

}